cmake_minimum_required(VERSION 3.0)
project(RMT Fortran)

# Create directory to house all compiled module files
set(CMAKE_Fortran_MODULE_DIRECTORY "mod")

# Set the list of source files
set(source
  "angular_momentum.f90"
  "calculation_parameters.f90"
  "checkpoint.f90"
  "communications_parameters.f90"
  "coordinate_system.f90"
  "coupling_rules.f90"
  "distribute_hd_blocks2.f90"
  "distribute_hd_blocks.f90"
  "distribute_wv_data.f90"
  "eigenstates_in_kryspace.f90"
  "electric_field.f90"
  "fftpack.f90"
  "file_num.f90"
  "finalise.f90"
  "global_data.f90"
  "global_linear_algebra.f90"
  "grid_parameters.f90"
  "hamiltonian_input_file.f90"
  "initial_conditions.f90"
  "initialise.f90"
  "inner_parallelisation.f90"
  "inner_propagators.f90"
  "inner_to_outer_interface.f90"
  "io_files.f90"
  "io_parameters.f90"
  "io_routines.f90"
  "kernel.f90"
  "kryspace_taylor_sums.f90"
  "live_communications.f90"
  "local_ham_matrix.f90"
  "lrpots.f90"
  "mpi_communications.F90"
  "mpi_layer_lblocks.f90"
  "outer_hamiltonian_atrlessthanb.f90"
  "outer_hamiltonian.f90"
  "outer_to_inner_interface.f90"
  "postprocessing.F90"
  "potentials.f90"
  "precisn.f90"
  "outer_propagators.f90"
  "ql_eigendecomposition.f90"
  "readhd.f90"
  "rmt_assert.f90"
  "serial_matrix_algebra.f90"
  "serialiseHD.f90"
  "setup_bspline_basis.f90"
  "setup_wv_data.f90"
  "splines.f90"
  "stages.f90"
  "tdse_dependencies.f90"
  "two_electron_outer_hamiltonian.f90"
  "utilities.f90"
  "version_control.F90"
  "wall_clock.f90"
  "wavefunction.f90"
  "work_at_intervals.f90"
)

# Define the module library
add_library(modules ${source})

# Retrieve version information from the repository
option(WITH_GIT "Add Git revision to program header (requires Git)" ON)
if (WITH_GIT)
    find_package(Git)
    if (Git_FOUND)
        execute_process(
            COMMAND "${GIT_EXECUTABLE}" log -1 --pretty=format:"%an"
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            OUTPUT_VARIABLE RMT_GIT_AUTH
            OUTPUT_STRIP_TRAILING_WHITESPACE
        )
        execute_process(
            COMMAND "${GIT_EXECUTABLE}" log -1 --pretty=format:"%h"
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            OUTPUT_VARIABLE RMT_GIT_HASH
            OUTPUT_STRIP_TRAILING_WHITESPACE
        )
        execute_process(
            COMMAND "${GIT_EXECUTABLE}" log -1 --pretty=format:"%ad"
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            OUTPUT_VARIABLE RMT_GIT_DATE
            OUTPUT_STRIP_TRAILING_WHITESPACE
        )
        message("-- Last commit author: ${RMT_GIT_AUTH}")
        message("-- Last commit hash: ${RMT_GIT_HASH}")
        message("-- Last commit date: ${RMT_GIT_DATE}")
        set_source_files_properties(version_control.F90
            PROPERTIES
            COMPILE_DEFINITIONS "GIT_AUTH=${RMT_GIT_AUTH};GIT_DATE=${RMT_GIT_DATE};GIT_HASH=${RMT_GIT_HASH}"
        )
    endif (Git_FOUND)
endif (WITH_GIT)
