! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Module to find eigen pairs for matrices by the QL method.
MODULE ql_eigendecomposition

    USE precisn, only: wp

    IMPLICIT NONE

CONTAINS

!---------------------------------------------------------------------------
! Finds SQRT(a**2+b**2) without overflow or destructive underflow
!---------------------------------------------------------------------------

    REAL(wp) FUNCTION pythag(a, b)

        REAL(wp) :: a, b
        REAL(wp) :: p, r, s, t, u

        INTEGER, PARAMETER :: max_no_of_iterations = 200
        INTEGER :: iteration_id

        p = MAX(ABS(a), ABS(b))

        IF (p == 0.0_wp) THEN
            pythag = 0.0_wp
            RETURN
        END IF

        r = (MIN(ABS(a), ABS(b))/p)**2

        DO iteration_id = 1, max_no_of_iterations

            t = r + 4.0_wp

            IF (t == 4.0_wp) THEN
                pythag = p
                RETURN
            END IF

            s = r/t
            u = s*2.0_wp + 1.0_wp
            p = u*p
            r = (s/u)**2*r

        END DO

        pythag = p
        RETURN

    END FUNCTION pythag


!---------------------------------------------------------------------------
!  SUBROUTINE QL_eigvals_and_vecs(n,d,e,z,ierr)  (eispack's tql2)
!
!     this subroutine is a translation of the algol procedure tql2,
!     num. math. 11, 293-306(1968) by bowdler, martin, reinsch, and
!     wilkinson.
!     handbook for auto. comp., vol.ii-linear algebra, 227-240(1971).
!
!     this subroutine finds the eigenvalues and eigenvectors
!     of a symmetric tridiagonal matrix by the ql method.
!     the eigenvectors of a full symmetric matrix can also
!     be found if  tred2  has been used to reduce this
!     full matrix to tridiagonal form.
!
!     on input
!
!    n is the order of the matrix.
!
!    d contains the diagonal elements of the input matrix.
!
!    e contains the subdiagonal elements of the input matrix
!      in its last n-1 positions.  e(1) is arbitrary.
!
!    z initially contain can be anything
!
!      on output
!
!    d contains the eigenvalues in ascending order.  if an
!      error exit is made, the eigenvalues are correct but
!      unordered for indices 1,2,...,ierr-1.
!
!    e has been destroyed.
!
!    z contains orthonormal eigenvectors of the symmetric
!      tridiagonal (or full) matrix.  if an error exit is made,
!      z contains the eigenvectors associated with the stored
!      eigenvalues.
!
!    ierr is set to
!      zero       for normal RETURN,
!      j          if the j-th eigenvalue has not been
!                 determined after 30 iterations.
!
!     calls pythag for  SQRT(a*a + b*b) .
!---------------------------------------------------------------------------

    SUBROUTINE ql_eigvals_and_vecs(n, d, e, z, ierr)

        INTEGER, INTENT(IN)       :: n
        REAL(wp), INTENT(INOUT)  :: d(n), e(n)
        REAL(wp), INTENT(OUT)    :: z(n, n)

        INTEGER    :: ierr
        INTEGER    :: eig_id, eig_id_1, eig_id_2
        REAL(wp)   :: d_eig_1, e_eig_1
        INTEGER    :: i, ii, k, m, j
        REAL(wp)   :: c, c2, c3, f, g, h, p, r, s, s2, tst1, tst2

        INTEGER, PARAMETER :: max_iteration = 40
        INTEGER    :: iteration_id

        ! z is OUT so must initialize to identity:
        z = 0.0_wp
        DO i = 1, n
            z(i, i) = 1.0_wp
        END DO

        ierr = 0
        IF (n .EQ. 1) RETURN

        DO i = 2, n
            e(i - 1) = e(i)
        END DO
        e(n) = 0.0_wp  !important

        f = 0.0_wp
        tst1 = 0.0_wp

        DO eig_id = 1, n

            Iteration_id = 0

            h = ABS(d(eig_id)) + ABS(e(eig_id))
            IF (tst1 .LT. h) tst1 = h

            ! look for small sub-diagonal element
            DO m = eig_id, n
                tst2 = tst1 + ABS(e(m))
                IF (tst2 .EQ. tst1) GO TO 120
            END DO

  120       IF (m .EQ. eig_id) GO TO 220

  130       IF (iteration_id .EQ. max_iteration) THEN
                ierr = eig_id
                RETURN
            END IF
            iteration_id = iteration_id + 1

            ! form shift 
            eig_id_1 = eig_id + 1
            eig_id_2 = eig_id + 2
            g = d(eig_id)
            p = (d(eig_id_1) - g)/(2.0_wp*e(eig_id))
            r = pythag(p, 1.0_wp)
            d(eig_id) = e(eig_id)/(p + SIGN(r, p))
            d(eig_id_1) = e(eig_id)*(p + SIGN(r, p))
            d_eig_1 = d(eig_id_1)
            h = g - d(eig_id)

            IF (eig_id_2 .le. n) THEN
                DO i = eig_id_2, n
                    d(i) = d(i) - h
                END DO
            END IF

            f = f + h

            ! ql transformation 
            p = d(m)
            c = 1.0_wp
            c2 = c
            e_eig_1 = e(eig_id_1)
            s = 0.0_wp

            DO i = m - 1, eig_id, -1
                c3 = c2
                c2 = c
                s2 = s
                g = c*e(i)
                h = c*p
                r = pythag(p, e(i))
                e(i + 1) = s*r
                s = e(i)/r
                c = p/r
                p = c*d(i) - s*g
                d(i + 1) = h + s*(c*g + s*d(i))

                ! form vector 
                DO k = 1, n
                    h = z(k, i + 1)
                    z(k, i + 1) = s*z(k, i) + c*h
                    z(k, i) = c*z(k, i) - s*h
                END DO

            END DO

            p = -s*s2*c3*e_eig_1*e(eig_id)/d_eig_1
            e(eig_id) = s*p
            d(eig_id) = c*p
            tst2 = tst1 + ABS(e(eig_id))
            IF (tst2 .GT. tst1) GO TO 130
  220       d(eig_id) = d(eig_id) + f
        END DO

        ! order eigenvalues and eigenvectors 

        DO ii = 2, n
            i = ii - 1
            k = i
            p = d(i)

            DO j = ii, n
                IF (d(j) .lt. p) THEN
                    k = j
                    p = d(j)
                END IF
            END DO

            IF (k .ne. i) THEN
                d(k) = d(i)
                d(i) = p

                DO j = 1, n
                    p = z(j, i)
                    z(j, i) = z(j, k)
                    z(j, k) = p
                END DO
            END IF

        END DO

        RETURN

    END SUBROUTINE ql_eigvals_and_vecs

END MODULE ql_eigendecomposition

