! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> \page reform_wavefunction
!> Program to read wavefunction data files and reconstruct the wavefunction
!> channel files in both the inner and outer regions. There are also some
!> optional post-processing steps that will be executed when enabled by a command-line
!> switch.
!>
!> The reconstructed wave function is written to files "OuterWaveXXXX", while the
!> additional optional data to "OuterWave_density" and "OuterWave_momentum".
!> Density and momentum spectrum are evaluated only in the polarization plane.
!> When evaluation in a different plane is desired, it is enough to change the
!> plane specification in the input configuration file before running this program.
!>
!> In molecular mode, program only reconstructs the outer wave function,
!> not the inner one.
!>
!> Executable should be run from either ground or state directories, as it looks
!> in the parent directory for input files.
!>
!> There are several command line options that can be used to control 
!> processing of the outer wave function by `reform`.
!>
!> When the `--density` switch is used, `reform` will also write
!> outer region position density distribution, \f$ \rho(\textbf{r})\f$, to a file.
!> 
!> In atomic calculations, the position density is given by 
!> 
!> \f{equation}{
!>     \rho(\textbf{r}) = \sum_{i L_i M_i} \left | \sum_{\ell m L M_L} 
!>     (L_i M_i \ell m|L M_L)
!>     \mathrm{i}^\ell F_{L_i M_i \ell m  L  M_L}(r)
!>     !{\mathcal{Y}}_{\ell}^{m}(\hat{\textbf{r}}) \right | ^2 .
!>     \label{momdiseq}
!> \f}
!>
!> Here \f$ i\f$ is the index of a residual-ion state with total orbital angular
!> momentum \f$ L_i \f$, and \f$ M_i\f$ is a projection of that angular momentum.
!> \f$L\f$ and \f$\ell\f$ are the total angular momenta of the
!> \f$(N+1)\f$-electron system and the outgoing electron respectively, and
!> \f$M_L\f$ and \f$m\f$ are their projections. \f$(L_i M_i \ell m|L M_L)\f$ is a
!> Clebsch-Gordan coefficient, \f${\mathcal{Y}}_{\ell} ^m \f$ is a complex
!> spherical harmonic conforming to the Fano-Racah phase convention, and \f$
!> F_p(r)\f$ is the reduced radial wave function of the ejected electron moving in
!> the channel with quantum numbers \f$p\f$ [5](10.1103/PhysRevA.79.053411). 
!>
!>  For the molecular case, we have instead
!>
!>  \f{equation}{
!>      \rho(\textbf{r}) = \sum_i \left | \sum_{\ell m} F_{i, \ell m}(r)
!>      Y_{\ell}^{m}(\hat{\textbf{r}}) \right |  ^2,
!>      \label{molecule_density}
!>  \f}
!>  where \f$ i \f$ is the index of the residual-ion state and, at variance with
!>  the atomic case, the \f$ Y_{\ell}^{m} \f$ are <i> real </i> spherical
!>  harmonics.  For convenience of notation, we have omitted all spin quantum
!>  numbers. 
!> 
!> With the `--momentum` switch, `reform` will produce data for the momentum
!> density-distribution \f$\rho(\textbf{k})\f$.  The momentum densities use the
!> same formulae given in the above equations, but with ejected-electron radial
!> wave functions \f$F_p(r)\f$ replaced by their Fourier transforms
!> \f$\tilde{F}_p(k)\f$.  As a check of accuracy, the code calculates the channel
!> populations in both position and momentum spaces, aiming to verify norm
!> conservation by the Fourier transform, and to recover the total outer-region
!> population upon summation of all channel populations.  Note that the
!> normalization convention is such that to recover the total outer-region
!> population as printed by RMT, one would need to integrate the distributions
!>
!> \f$\rho(\textbf{r})\f$ and
!> \f$\rho(\textbf{k})\f$ as \f$\int r^{-2} \rho(\textbf{r}) \mathrm{d}^3 \textbf{r}\f$ and
!> \f$\int k^{-2} \rho(\textbf{k}) \mathrm{d}^3 \textbf{k}\f$, respectively.
!> 
!> The position and momentum density output files have the form of text files,
!> `OuterWave_density.txt` and `OuterWave_momentum.txt`, respectively, containing
!> matrices of densities evaluated per angle (rows) and per radial point (columns)
!> in the chosen sampling plane, which defaults to the polarization plane, and
!> where the angle runs in the same direction as the time-dependent polarization
!> vector.  The spacing between the radial samples in the position density file
!> is equal to the parameter `deltaR` from the RMT input namelist, and the
!> first sample corresponds to the density value at the R-matrix boundary. The
!> spacing between the radial samples in the momentum density file is equal to
!> \f$ \Delta k = 2\pi / (R_{\mathrm{max}} - R_{\mathrm{min}})\f$, where \f$
!> R_{\mathrm{max}}\f$ is the end of the outer region grid and \f$
!> R_{\mathrm{min}}\f$ is the smallest radius used in the Fourier transform.
!> 
!> Other command line options can be used to
!> 
!> * set the number of angular samples around the full circle: `--ntheta` <i>N </i>,
!>           default 360, resulting in one-degree resolution,
!> * set the maximal number of radial samples: `--nmaxpt` <i>N</i>, default 10000,
!>           also limited by the actual number of finite difference points in the outer region,
!> * select only specific channels of the wave function to include in processing:
!>           `--channels` <i>i,j,k,... </i>,
!> *  ignore part of the outer region wavefunction closest to the R-matrix boundary
!>           when evaluating the momentum distribution:  `--rskip` <i>R</i>, default 200 a.u.,
!> * choose a different orientation of the sampling plane: `--plane` \f$ \alpha,\beta,\gamma\f$,
!> * produce also a fully three-dimensional distribution in the Visualization Toolkit
!>           unstructured grid format: `--vtk`,
!> * stop early, after only calculating and writing the 1-D momentum/density spectrum:
!>           `--spectrum_only`.
!> * use Coulomb waves, rather than plane waves, for the computation of the momentum:
!>           `--coulomb`.
!> 
!>
!> The text files contain one more line in addition to the number given by
!> `--ntheta`; the first line is repeated at the end to allow continuous plotting.
!> The program also writes one extra output file for each residual-ion state (and
!> for each its magnetic sub-level in atomic cases), with partial photo-electron
!> distribution corresponding to ionization into that state.
!> 
!> The momentum distribution evaluation subroutines make use of the Netlib package [FFTPACK]
!> (https://www.netlib.org/fftpack/).
!>
!>
!> @brief program to read wavefunction data files and reconstruct the wavefunction
!> channel files in both the inner and outer regions.

PROGRAM reform

    USE precisn,            ONLY: wp
    USE splines,            ONLY: read_spline_files, &
                                  calc_and_write_wv, &
                                  reform_outer
    USE readhd,             ONLY: reform_reads, &
                                  read_psi_files, &
                                  rmatr
    USE initial_conditions, ONLY: deltaR, &
                                  get_disk_path, &
                                  read_initial_conditions, &
                                  molecular_target, &
                                  euler_alpha, &
                                  euler_beta, &
                                  euler_gamma
    USE io_parameters,      ONLY: version, &
                                  init_io_parameters
    USE version_control,    ONLY: print_program_header
    USE coordinate_system,  ONLY: print_coordinate_system
    USE electric_field,     ONLY: init_electric_field_module
    USE postprocessing,     ONLY: process_command_line, &
                                  reform_density, &
                                  reform_momentum

    IMPLICIT NONE

    COMPLEX(wp), ALLOCATABLE :: wv(:, :, :)  ! the outer wavefunction (points,channels,solutions)
    REAL(wp),    ALLOCATABLE :: alpha(:), beta(:), gamma(:)
    INTEGER,     ALLOCATABLE :: channels(:)

    INTEGER  :: num_exp_points_inner, ntheta = 360, maxpt = 10000
    LOGICAL  :: calc_density = .FALSE., calc_momentum = .FALSE., vtk = .FALSE., &
                spectrum_only = .FALSE., recon = .TRUE., coulomb = .FALSE.
    REAL(wp) :: skip = 200

    ! initialize; find and read input configuration
    CALL print_program_header
    CALL get_disk_path('../')
    CALL read_initial_conditions
    CALL init_io_parameters
    CALL init_electric_field_module
    CALL print_coordinate_system
    CALL process_command_line(calc_density, calc_momentum, ntheta, maxpt, skip, alpha, beta,&
                              gamma, channels, vtk, spectrum_only, recon, coulomb)
    CALL reform_reads

    ! reconstruct the inner wave function (atomic mode only)
    IF (.NOT. molecular_target) THEN

        ! Set the number of points on which to expand the inner region w.f.
        ! This is set to be consistent with the outer region, but can be increased
        ! for better resolution
        num_exp_points_inner = INT(rmatr/deltaR) + 1

        CALL read_spline_files(num_exp_points_inner)
        CALL read_psi_files(version)
        CALL calc_and_write_wv(num_exp_points_inner, recon)

    END IF

    ! reconstruct the outer wave function
    CALL reform_outer(version, recon, wv)

    ! change coordinate system according to the command line (or use polarization plane if not specified)
    euler_alpha = alpha
    euler_beta  = beta
    euler_gamma = gamma

    ! optional post-processing of the outer wave function
    IF (calc_density)  CALL reform_density(wv, channels, ntheta, maxpt, vtk, spectrum_only)
    IF (calc_momentum) CALL reform_momentum(wv, channels, ntheta, maxpt, vtk, spectrum_only, skip, coulomb)

    WRITE (*, '(/,1x,"Done.")')

END PROGRAM reform
