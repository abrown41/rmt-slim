"""
Utility for pulling test data from zenodo.

In order to keep the size of the repository small, larger test data files are
housed on zenodo. If you require the data on your machine for testing, then you
can pull it with `RMT_fetch_data`. It will present you with options for which
test data you need, or you can specify to download all data with

`RMT_fetch_data -a`
"""
import io
import requests
import tarfile
import tqdm


def read_command_line():
    from argparse import ArgumentParser as AP
    parser = AP()
    parser.add_argument('-a', '--all', help='fetch all available test data',
                        action='store_true', default=False)

    return parser.parse_args()


def get_record_files(id):
    """ Pull json data object from the referenced zenodo record, and extract the
    name of each tar file for download, as well as the url for fetching it"""
    data = requests.get(f"https://zenodo.org/api/records/{id}")
    assert data.status_code == 200
    json = data.json()

    return [(f["key"], f["links"]["self"]) for f in json["files"]]


def get_file(name, url):
    request = requests.get(url, stream=True)
    assert request.status_code == 200
    file_size = int(request.headers.get('Content-Length', 0))

    desc = "(Unknown total file size)" if file_size == 0 else f"Downloading {name}"

    bio = io.BytesIO()
    with tqdm.tqdm(
        desc=desc,
        total=file_size,
        unit='b',
        unit_scale=True,
        unit_divisor=1024,
    ) as bar:
        for chunk in request.iter_content(chunk_size=65536):
            bar.update(len(chunk))
            bio.write(chunk)

    bio.seek(0)
    return bio


def select_files(files):
    """given a list of files, present a list for selection"""
    from inquirer import Checkbox, prompt

    questions = [Checkbox('file',
                          message=" Which test data do you want to download? \
(<up>/<down> to navigate, \
<space> to check/uncheck, \
<enter> to confirm)",
                          choices=[(file[0], file) for file in files])]
    # Checkbox interprets a list of tuples as the display name and the return
    # name, we want both the name and url, so we replace the (name, url)
    # tuple in the choices with (name, (name, url))

    answers = prompt(questions)
    return answers["file"]


def main():
    from rmt_utilities.zenodo_record import RECORD_ID

    args = read_command_line()
    files = get_record_files(RECORD_ID)
    if args.all:
        selected_files = files
    else:
        selected_files = select_files(files)

    if selected_files:
        for file in selected_files[0]:
            assert file.endswith(".tar.gz")

        for (name, url) in selected_files:
            try:
                content = get_file(name, url)
                tar = tarfile.open(fileobj=content)

                print(f"Extracting {name}")
                tar.extractall()
            except KeyboardInterrupt:
                print("Fetch Aborted")
                from sys import exit
                exit()
    else:
        print("Nothing to fetch!")


if __name__ == "__main__":
    main()
