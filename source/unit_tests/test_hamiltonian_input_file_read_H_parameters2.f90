MODULE test_hamiltonian_input_file_read_H_parameters2
    USE testdrive, ONLY: check, error_type, new_unittest, test_failed, unittest_type

    USE precisn, ONLY: wp
    USE hamiltonian_input_file, ONLY: read_H_parameters2, read_H_file2
    USE rmt_utilities, ONLY: int_to_char

    USE testing_utilities, ONLY: compare, data_file, format_context

    IMPLICIT NONE

    PRIVATE
    PUBLIC :: collect_hamiltonian_input_file_read_H_parameters2
CONTAINS
    SUBROUTINE collect_hamiltonian_input_file_read_H_parameters2(testsuite)
        TYPE(unittest_type), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: testsuite

        testsuite = [new_unittest("atomic::small::Ne_4cores", test_atomic_small_Ne_4cores), &
                     new_unittest("atomic::small::ar+", test_atomic_small_arx), &
                     new_unittest("atomic::small::argon", test_atomic_small_argon), &
                     new_unittest("atomic::small::helium", test_atomic_small_helium), &
                     new_unittest("atomic::small::iodine", test_atomic_small_iodine), &
                     new_unittest("atomic::small:Ar_jK", test_atomic_small_Ar_jK)]
    END SUBROUTINE

    SUBROUTINE test_read_H_parameters_values(error, &
                                             path, &
                                             reduced_L_max, &
                                             coupling_id, &
                                             lplusp, &
                                             set_ML_max, &
                                             xy_plane_desired, &
                                             original_ML_max, &
                                             expected_nelc, &
                                             expected_nz, &
                                             expected_lrang2, &
                                             expected_lamax, &
                                             expected_ntarg, &
                                             expected_rmatr, &
                                             expected_bbloch, &
                                             expected_etarg, &
                                             expected_ltarg, &
                                             expected_starg, &
                                             expected_nchmx, &
                                             expected_nstmx, &
                                             expected_lmaxp1, &
                                             expected_no_of_L_blocks, &
                                             expected_no_of_LML_blocks, &
                                             expected_ML_max, &
                                             expected_kept, &
                                             expected_nstk, &
                                             expected_nchn, &
                                             expected_last_lrgl, &
                                             expected_inast)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
        CHARACTER(len=*), INTENT(IN) :: path

        INTEGER, INTENT(IN) :: reduced_L_max, coupling_id, lplusp
        LOGICAL, INTENT(IN) :: set_ML_max, xy_plane_desired
        INTEGER, INTENT(IN) :: original_ML_max

        INTEGER, INTENT(IN) :: expected_nelc, expected_nz, expected_lrang2, expected_lamax, expected_ntarg
        REAL(wp), INTENT(IN) :: expected_rmatr, expected_bbloch
        REAL(wp), DIMENSION(:), INTENT(IN) :: expected_etarg
        INTEGER, DIMENSION(:), INTENT(IN) :: expected_ltarg, expected_starg
        INTEGER, INTENT(IN) :: expected_nchmx, expected_nstmx, expected_ML_max, expected_lmaxp1, &
                               expected_no_of_L_blocks, expected_no_of_LML_blocks
        INTEGER, DIMENSION(:), INTENT(IN) :: expected_kept, expected_nstk, expected_nchn
        INTEGER, INTENT(IN) :: expected_last_lrgl, expected_inast

        INTEGER :: actual_nelc, actual_nz, actual_lrang2, actual_lamax, actual_ntarg
        REAL(wp) :: actual_rmatr, actual_bbloch
        REAL(wp), DIMENSION(:), ALLOCATABLE :: actual_etarg
        INTEGER, DIMENSION(:), ALLOCATABLE :: actual_ltarg, actual_starg
        INTEGER :: actual_nchmx, actual_nstmx, actual_ML_max, actual_lmaxp1, &
                   actual_no_of_L_blocks, actual_no_of_LML_blocks
        INTEGER, DIMENSION(:), ALLOCATABLE :: actual_kept, actual_nstk, actual_nchn
        INTEGER :: actual_last_lrgl, actual_inast
        ! NOTE: This parameter is just used for logging, so the value isn't tested.
        INTEGER :: ignored_L_max

        CHARACTER(len=:), ALLOCATABLE :: H_file_path
        CHARACTER(len=:), ALLOCATABLE :: context

        H_file_path = data_file(path)

        actual_ML_max = original_ML_max
        !$OMP CRITICAL
        ! These two functions allocate, set, and deallocate a global variable so need to avoid threading.
        CALL read_H_parameters2(path=H_file_path, &
                                reduced_L_max=reduced_L_max, &
                                coupling_id=coupling_id, &
                                lplusp=lplusp, &
                                set_ML_max=set_ML_max, &
                                xy_plane_desired=xy_plane_desired, &
                                nelc=actual_nelc, &
                                nz=actual_nz, &
                                lrang2=actual_lrang2, &
                                lamax=actual_lamax, &
                                ntarg=actual_ntarg, &
                                rmatr=actual_rmatr, &
                                bbloch=actual_bbloch, &
                                etarg=actual_etarg, &
                                ltarg=actual_ltarg, &
                                starg=actual_starg, &
                                nchmx=actual_nchmx, &
                                nstmx=actual_nstmx, &
                                lmaxp1=actual_lmaxp1, &
                                no_of_L_blocks=actual_no_of_L_blocks, &
                                no_of_LML_blocks=actual_no_of_LML_blocks, &
                                ML_max=actual_ML_max, &
                                kept=actual_kept, &
                                nstk=actual_nstk, &
                                nchn=actual_nchn, &
                                L_max=ignored_L_max, &
                                last_lrgl=actual_last_lrgl, &
                                inast=actual_inast)

        BLOCK
            INTEGER :: ignored_lamax, ignored_L_block_tot_nchan, ignored_max_L_block_size, ignored_LML_block_tot_nchan
            INTEGER, DIMENSION(:), ALLOCATABLE :: ignored_L_block_lrgl, ignored_L_block_nspn, ignored_L_block_npty, &
                                                  ignored_L_block_nchan, ignored_mnp1, ignored_neigsrem, ignored_L_block_post, &
                                                  ignored_states_per_L_block, ignored_LML_block_lrgl, ignored_LML_block_nspn, &
                                                  ignored_LML_block_npty, ignored_LML_block_nchan, ignored_LML_block_ml, &
                                                  ignored_LML_block_Lblk, ignored_LML_block_post, ignored_states_per_LML_block
            INTEGER, DIMENSION(:, :), ALLOCATABLE :: ignored_L_block_nconat, ignored_L_block_l2p, ignored_lrgt, &
                                                     ignored_LML_block_nconat, ignored_LML_block_l2p, ignored_m2p, ignored_lstartm1
            REAL(wp), DIMENSION(:, :), ALLOCATABLE :: ignored_eig
            REAL(wp), DIMENSION(:, :, :), ALLOCATABLE :: ignored_wmat
            REAL(wp), DIMENSION(:, :, :, :), ALLOCATABLE :: ignored_L_block_cf, ignored_LML_block_cf

            ignored_lamax = actual_lamax

            CALL read_H_file2(adjust_gs_energy=.FALSE., &
                              gs_finast=-1, &
                              gs_energy_desired=0.0_WP, &
                              remove_eigvals_threshold=.FALSE., &
                              no_of_L_blocks=actual_no_of_L_blocks, &
                              no_of_LML_blocks=actual_no_of_LML_blocks, &
                              nchmx=actual_nchmx, &
                              nstmx=actual_nstmx, &
                              lplusp=lplusp, &
                              set_ML_max=set_ML_max, &
                              ML_max=actual_ML_max, &
                              reduced_L_max=-1, &
                              reduced_lamax=-1, &
                              coupling_id=coupling_id, &
                              xy_plane_desired=xy_plane_desired, &
                              neigsrem_1st_sym=14, &
                              neigsrem_higher_syms=20, &
                              surf_amp_threshold_value=1.0_WP, &
                              lamax=ignored_lamax, &
                              L_block_tot_nchan=ignored_L_block_tot_nchan, &
                              L_block_lrgl=ignored_L_block_lrgl, &
                              L_block_nspn=ignored_L_block_nspn, &
                              L_block_npty=ignored_L_block_npty, &
                              L_block_nchan=ignored_L_block_nchan, &
                              mnp1=ignored_mnp1, &
                              neigsrem=ignored_neigsrem, &
                              L_block_post=ignored_L_block_post, &
                              states_per_L_block=ignored_states_per_L_block, &
                              max_L_block_size=ignored_max_L_block_size, &
                              L_block_nconat=ignored_L_block_nconat, &
                              L_block_l2p=ignored_L_block_l2p, &
                              lrgt=ignored_lrgt, &
                              eig=ignored_eig, &
                              wmat=ignored_wmat, &
                              L_block_cf=ignored_L_block_cf, &
                              LML_block_lrgl=ignored_LML_block_lrgl, &
                              LML_block_nspn=ignored_LML_block_nspn, &
                              LML_block_npty=ignored_LML_block_npty, &
                              LML_block_nchan=ignored_LML_block_nchan, &
                              LML_block_tot_nchan=ignored_LML_block_tot_nchan, &
                              LML_block_ml=ignored_LML_block_ml, &
                              LML_block_Lblk=ignored_LML_block_Lblk, &
                              LML_block_post=ignored_LML_block_post, &
                              states_per_LML_block=ignored_states_per_LML_block, &
                              LML_block_nconat=ignored_LML_block_nconat, &
                              LML_block_l2p=ignored_LML_block_l2p, &
                              m2p=ignored_m2p, &
                              lstartm1=ignored_lstartm1, &
                              LML_block_cf=ignored_LML_block_cf)
        END BLOCK
        !$OMP END CRITICAL

        context = ""

        IF (.NOT. compare(actual_nelc, expected_nelc)) THEN
            CALL format_context(context, "nelc", actual_nelc, expected_nelc)
        END IF

        IF (.NOT. compare(actual_nz, expected_nz)) THEN
            CALL format_context(context, "nz", actual_nz, expected_nz)
        END IF

        IF (.NOT. compare(actual_lrang2, expected_lrang2)) THEN
            CALL format_context(context, "lrang2", actual_lrang2, expected_lrang2)
        END IF

        IF (.NOT. compare(actual_lamax, expected_lamax)) THEN
            CALL format_context(context, "lamax", actual_lamax, expected_lamax)
        END IF

        IF (.NOT. compare(actual_ntarg, expected_ntarg)) THEN
            CALL format_context(context, "ntarg", actual_ntarg, expected_ntarg)
        END IF

        IF (.NOT. compare(actual_rmatr, expected_rmatr)) THEN
            CALL format_context(context, "rmatr", actual_rmatr, expected_rmatr)
        END IF

        IF (.NOT. compare(actual_bbloch, expected_bbloch)) THEN
            CALL format_context(context, "bbloch", actual_bbloch, expected_bbloch)
        END IF

        IF (.NOT. compare(actual_etarg, expected_etarg)) THEN
            CALL format_context(context, "etarg", actual_etarg, expected_etarg)
        END IF

        IF (.NOT. compare(actual_ltarg, expected_ltarg)) THEN
            CALL format_context(context, "ltarg", actual_ltarg, expected_ltarg)
        END IF

        IF (.NOT. compare(actual_starg, expected_starg)) THEN
            CALL format_context(context, "starg", actual_starg, expected_starg)
        END IF

        IF (.NOT. compare(actual_nchmx, expected_nchmx)) THEN
            CALL format_context(context, "nchmx", actual_nchmx, expected_nchmx)
        END IF

        IF (.NOT. compare(actual_nstmx, expected_nstmx)) THEN
            CALL format_context(context, "nstmx", actual_nstmx, expected_nstmx)
        END IF

        IF (.NOT. compare(actual_ML_max, expected_ML_max)) THEN
            CALL format_context(context, "ML_max", actual_ML_max, expected_ML_max)
        END IF

        IF (.NOT. compare(actual_lmaxp1, expected_lmaxp1)) THEN
            CALL format_context(context, "lmaxp1", actual_lmaxp1, expected_lmaxp1)
        END IF

        IF (.NOT. compare(actual_no_of_L_blocks, expected_no_of_L_blocks)) THEN
            CALL format_context(context, "no_of_L_blocks", actual_no_of_L_blocks, expected_no_of_L_blocks)
        END IF

        IF (.NOT. compare(actual_no_of_LML_blocks, expected_no_of_LML_blocks)) THEN
            CALL format_context(context, "no_of_LML_blocks", actual_no_of_LML_blocks, expected_no_of_LML_blocks)
        END IF

        IF (.NOT. compare(actual_kept, expected_kept)) THEN
            CALL format_context(context, "kept", actual_kept, expected_kept)
        END IF

        IF (.NOT. compare(actual_nstk, expected_nstk)) THEN
            CALL format_context(context, "nstk", actual_nstk, expected_nstk)
        END IF

        IF (.NOT. compare(actual_nchn, expected_nchn)) THEN
            CALL format_context(context, "nchn", actual_nchn, expected_nchn)
        END IF

        IF (.NOT. compare(actual_last_lrgl, expected_last_lrgl)) THEN
            CALL format_context(context, "last_lrgl", actual_last_lrgl, expected_last_lrgl)
        END IF

        IF (.NOT. compare(actual_inast, expected_inast)) THEN
            CALL format_context(context, "inast", actual_inast, expected_inast)
        END IF

        IF (context /= "") THEN
            CALL test_failed(error, "H parameters set incorrectly!", context)
        END IF
    END SUBROUTINE

    SUBROUTINE test_atomic_small_Ne_4cores(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL test_read_H_parameters_values(error=error, &
                                           path="unit_test_data/Ne_H", &
                                           reduced_L_max=-1, &
                                           coupling_id=1, &
                                           lplusp=0, &
                                           set_ML_max=.TRUE., &
                                           xy_plane_desired=.FALSE., &
                                           original_ML_max=0, &
                                           expected_nelc=9, &
                                           expected_nz=10, &
                                           expected_lrang2=4, &
                                           expected_lamax=4, &
                                           expected_ntarg=2, &
                                           expected_rmatr=2.000000E+01_WP, &
                                           expected_bbloch=0.000000E+00_WP, &
                                           expected_etarg=[-1.279277E+02_WP, -1.269274E+02_WP], &
                                           expected_ltarg=[1, 0], &
                                           expected_starg=[2, 2], &
                                           expected_nchmx=3, &
                                           expected_nstmx=254, &
                                           expected_lmaxp1=3, &
                                           expected_no_of_L_blocks=3, &
                                           expected_no_of_LML_blocks=3, &
                                           expected_ML_max=0, &
                                           expected_kept=[1, 0, 1, 1, 0], &
                                           expected_nstk=[134, 141, 233, 254, 165], &
                                           expected_nchn=[2, 3, 3], &
                                           expected_last_lrgl=2, &
                                           expected_inast=5)
    END SUBROUTINE

    SUBROUTINE test_atomic_small_arx(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL test_read_H_parameters_values(error=error, &
                                           path="unit_test_data/Ar+_H", &
                                           reduced_L_max=-1, &
                                           coupling_id=1, &
                                           lplusp=0, &
                                           set_ML_max=.FALSE., &
                                           xy_plane_desired=.FALSE., &
                                           original_ML_max=-1, &
                                           expected_nelc=16, &
                                           expected_nz=18, &
                                           expected_lrang2=6, &
                                           expected_lamax=4, &
                                           expected_ntarg=1, &
                                           expected_rmatr=1.500000E+01_WP, &
                                           expected_bbloch=0.000000E+00_WP, &
                                           expected_etarg=[-5.252291E+02_WP], &
                                           expected_ltarg=[2], &
                                           expected_starg=[1], &
                                           expected_nchmx=3, &
                                           expected_nstmx=141, &
                                           expected_lmaxp1=4, &
                                           expected_no_of_L_blocks=7, &
                                           expected_no_of_LML_blocks=31, &
                                           expected_ML_max=3, &
                                           expected_kept=[1, 1, 1, 1, 1, 1, 1], &
                                           expected_nstk=[49, 48, 95, 141, 94, 94, 139], &
                                           expected_nchn=[1, 1, 2, 3, 2, 2, 3], &
                                           expected_last_lrgl=3, &
                                           expected_inast=7)
    END SUBROUTINE

    SUBROUTINE test_atomic_small_argon(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL test_read_H_parameters_values(error=error, &
                                           path="unit_test_data/Ar_H", &
                                           reduced_L_max=-1, &
                                           coupling_id=1, &
                                           lplusp=0, &
                                           set_ML_max=.FALSE., &
                                           xy_plane_desired=.FALSE., &
                                           original_ML_max=-1, &
                                           expected_nelc=17, &
                                           expected_nz=18, &
                                           expected_lrang2=4, &
                                           expected_lamax=4, &
                                           expected_ntarg=2, &
                                           expected_rmatr=2.000000E+01_WP, &
                                           expected_bbloch=0.000000E+00_WP, &
                                           expected_etarg=[-5.263857E+02_WP, -5.258874E+02_WP], &
                                           expected_ltarg=[1, 0], &
                                           expected_starg=[2, 2], &
                                           expected_nchmx=3, &
                                           expected_nstmx=253, &
                                           expected_ML_max=2, &
                                           expected_lmaxp1=3, &
                                           expected_no_of_L_blocks=5, &
                                           expected_no_of_LML_blocks=17, &
                                           expected_kept=[1, 1, 1, 1, 1], &
                                           expected_nstk=[148, 110, 236, 253, 127], &
                                           expected_nchn=[2, 1, 3, 3, 1], &
                                           expected_last_lrgl=2, &
                                           expected_inast=5)
    END SUBROUTINE

    SUBROUTINE test_atomic_small_helium(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL test_read_H_parameters_values(error=error, &
                                           path="unit_test_data/He_H", &
                                           reduced_L_max=-1, &
                                           coupling_id=1, &
                                           lplusp=0, &
                                           set_ML_max=.TRUE., &
                                           xy_plane_desired=.FALSE., &
                                           original_ML_max=0, &
                                           expected_nelc=1, &
                                           expected_nz=2, &
                                           expected_lrang2=5, &
                                           expected_lamax=4, &
                                           expected_ntarg=3, &
                                           expected_rmatr=1.500000E+01_WP, &
                                           expected_bbloch=0.000000E+00_WP, &
                                           expected_etarg=[-2.000000E+00_WP, 4.914448E-09_WP, 6.666667E-01_WP], &
                                           expected_ltarg=[0, 1, 0], &
                                           expected_starg=[2, 2, 2], &
                                           expected_nchmx=4, &
                                           expected_nstmx=154, &
                                           expected_ML_max=0, &
                                           expected_lmaxp1=4, &
                                           expected_no_of_L_blocks=4, &
                                           expected_no_of_LML_blocks=4, &
                                           expected_kept=[1, 0, 1, 1, 0, 0, 1], &
                                           expected_nstk=[118, 38, 154, 152, 38, 37, 148], &
                                           expected_nchn=[3, 4, 4, 4], &
                                           expected_last_lrgl=3, &
                                           expected_inast=7)
    END SUBROUTINE

    SUBROUTINE test_atomic_small_iodine(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        CALL test_read_H_parameters_values(error=error, &
                                           path="unit_test_data/I_H", &
                                           reduced_L_max=7, &
                                           coupling_id=2, &
                                           lplusp=0, &
                                           set_ML_max=.TRUE., &
                                           xy_plane_desired=.FALSE., &
                                           original_ML_max=3, &
                                           expected_nelc=52, &
                                           expected_nz=53, &
                                           expected_lrang2=11, &
                                           expected_lamax=10, &
                                           expected_ntarg=3, &
                                           expected_rmatr=20.0_WP, &
                                           expected_bbloch=0.0_WP, &
                                           expected_etarg=[-6.917447e+03_WP, -6.917418e+03_WP, -6.917415e+03_WP], &
                                           expected_ltarg=[4, 0, 2], &
                                           expected_starg=[0, 0, 0], &
                                           expected_nchmx=9, &
                                           expected_nstmx=396, &
                                           expected_ML_max=3, &
                                           expected_lmaxp1=6, &
                                           expected_no_of_L_blocks=6, &
                                           expected_no_of_LML_blocks=20, &
                                           expected_kept=[1, 1, 1, 1, 1, 1], &
                                           expected_nstk=[220, 221, 352, 353, 396, 396], &
                                           expected_nchn=[5, 5, 8, 8, 9, 9], &
                                           expected_last_lrgl=5, &
                                           expected_inast=6)
    END SUBROUTINE

    SUBROUTINE test_atomic_small_Ar_jK(error)
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error

        ! NOTE: This test is based on data that has not been confirmed to be inputs to a successful calculation, therefore some
        ! values may be non-sensical or incorrect.
        CALL test_read_H_parameters_values(error=error, &
                                           path="unit_test_data/Ar_jK_H", &
                                           reduced_L_max=-1, &
                                           coupling_id=2, &
                                           lplusp=0, &
                                           set_ML_max=.FALSE., &
                                           xy_plane_desired=.FALSE., &
                                           original_ML_max=-1, &
                                           expected_nelc=17, &
                                           expected_nz=18, &
                                           expected_lrang2=11, &
                                           expected_lamax=8, &
                                           expected_ntarg=2, &
                                           expected_rmatr=2.000000E+01_WP, &
                                           expected_bbloch=0.000000E+00_WP, &
                                           expected_etarg=[-5.262749E+02_WP, -5.262684E+02_WP], &
                                           expected_ltarg=[3, 1], &
                                           expected_starg=[1, 1], &
                                           expected_nchmx=6, &
                                           expected_nstmx=318, &
                                           expected_ML_max=4, &
                                           expected_lmaxp1=5, &
                                           expected_no_of_L_blocks=6, &
                                           expected_no_of_LML_blocks=18, &
                                           expected_kept=[1, 1, 1, 1, 1, 1], &
                                           expected_nstk=[97, 104, 239, 264, 286, 318], &
                                           expected_nchn=[2, 2, 5, 5, 6, 6], &
                                           expected_last_lrgl=4, &
                                           expected_inast=6)
    END SUBROUTINE
END MODULE test_hamiltonian_input_file_read_H_parameters2
